﻿using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Text;

namespace FusionBetFeed.Services
{
    public class CacheService : ICacheService
    {
        private IMemoryCache cache;
        public CacheService(IMemoryCache cache)
        {
            this.cache = cache;
        }
        public bool Add<T>(string key, T value, int? minutes)
        {
            try
            {
                // Set cache options.
                MemoryCacheEntryOptions cacheEntryOptions = null;
                if (minutes.HasValue)
                {
                    cacheEntryOptions = new MemoryCacheEntryOptions()
                  .SetAbsoluteExpiration(TimeSpan.FromMinutes(minutes.Value));
                }


                // Save data in cache.
                if (cacheEntryOptions != null)
                {
                    //if there are cache options, set them
                    cache.Set(key, value, cacheEntryOptions);
                }
                else
                {
                    //otherwise set for indefinite time
                    cache.Set(key, value);
                }


                return true;
            }
            catch (Exception ex)
            {

                //TODO: log error
                return false;
            }

        }

        public bool Add<T>(string key, T value)
        {
            return this.Add<T>(key, value, null);
        }

        public bool Exists(string key)
        {
            throw new NotImplementedException();
        }

        public T Get<T>(string key)
        {
            try
            {


                // Look for cache key.
                T cacheEntry = cache.Get<T>(key);
                return cacheEntry;

            }
            catch (Exception ex)
            {
                //TODO: log error
                return default(T);
            }

        }

        public bool Remove(string key)
        {
            try
            {
                cache.Remove(key);
                return true;
            }
            catch (Exception ex)
            {

                //TODO: log error
                return false;
            }
        }

        public void RemoveByKeyStart(string key)
        {
            throw new NotImplementedException();
        }
    }
}
