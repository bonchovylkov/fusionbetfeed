﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FusionBetFeed.Services
{
   public interface ICacheService
    {
        /// <summary>
        /// Adds new item to the cache
        /// </summary>
        /// <typeparam name="T">The type of the item that is being added</typeparam>
        /// <param name="key">The unique key of the stored item</param>
        /// <param name="value">The value that is being stored</param>
        /// <param name="minutes">Expiratio minites of the cache</param>
        /// <returns>If the item is being stored succesfully</returns>
        bool Add<T>(string key, T value, int? minutes);

        /// <summary>
        /// Adds new item to the cache with no expiration date
        /// </summary>
        /// <typeparam name="T">The type of the item that is being added</typeparam>
        /// <param name="key">The unique key of the stored item</param>
        /// <param name="value">The value that is being stored</param>
        /// <returns>If the item is being stored succesfully</returns>
        bool Add<T>(string key, T value);

        /// <summary>
        /// Retrieves item from the cache by key
        /// </summary>
        /// <typeparam name="T">The type of the item</typeparam>
        /// <param name="key">The unique key to search for</param>
        /// <returns>The item if been found</returns>
        T Get<T>(string key);

        /// <summary>
        /// Removes item by key
        /// </summary>
        /// <param name="key">The unique key to search for</param>
        /// <returns>If the removal is succesfull</returns>
        bool Remove(string key);

        /// <summary>
        /// Removes all items that start with specific key
        /// </summary>
        /// <param name="key"></param>
        void RemoveByKeyStart(string key);

        /// <summary>
        /// Check if item exists in the cache
        /// </summary>
        /// <param name="key">The unique key to search for</param>
        /// <returns>Returns rather the key exists or not</returns>
        bool Exists(string key);
    }
}
