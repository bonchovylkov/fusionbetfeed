﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FusionBetFeed.Data.Common.Repositories;
using FusionBetFeed.Data.Models;

namespace FusionBetFeed.Services.Data
{
    public class SportService : ISportService
    {
        IDeletableEntityRepository<Sport> sportsRepo;
        public SportService(IDeletableEntityRepository<Sport> sportsRepo)
        {
            this.sportsRepo = sportsRepo;
        }
        public Sport GetSportByFeedId(int id)
        {
            return this.sportsRepo.All().FirstOrDefault(s => s.FeedId == id);
        }
    }
}
