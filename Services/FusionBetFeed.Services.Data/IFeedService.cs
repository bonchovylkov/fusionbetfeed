﻿using FusionBetFeed.Common.XMLMappings;
using FusionBetFeed.Web.Infrastructure.ViewModels.Feed;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FusionBetFeed.Services.Data
{
    public interface IFeedService
    {
        Task InitialFeed(XmlSportsModel xmlSportsModel);

        int AddEvent(EventModel eventModel);
        int AddMatch(MatchModel matchModel);
        int AddBet(BetModel betModel);
        int AddOdd(OddModel oddModel);
        List<MatchDefaultBetOddsViewModel> GetNextDayMatches(int skip, int take);
        List<OddViewModel> GetBetOdds(int id);
        Task AddNewEvents(List<EventModel> newEvents);
        Task AddUpdateEventsAndMatches(XmlSportsModel xmlSportsModel);

        List<MatchModel> GetMatchesForUpdate(XmlSportsModel newFeed, bool fullUpdate);
        FullMatchViewModel GetMatchById(int id);
    }
}
