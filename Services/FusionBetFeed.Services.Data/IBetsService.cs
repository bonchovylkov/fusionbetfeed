﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FusionBetFeed.Services.Data
{
    public interface IBetsService
    {
        int GetTotalBetsCount();
    }
}
