﻿using FusionBetFeed.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace FusionBetFeed.Services.Data
{
    public interface ISportService
    {
        Sport GetSportByFeedId(int id);
    }
}
