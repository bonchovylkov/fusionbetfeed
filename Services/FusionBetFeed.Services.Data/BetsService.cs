﻿using FusionBetFeed.Data.Common.Repositories;
using FusionBetFeed.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FusionBetFeed.Services.Data
{
    public class BetsService : IBetsService
    {
        IDeletableEntityRepository<Bet> betsRepo;
        public BetsService(IDeletableEntityRepository<Bet> betsRepo)
        {
            this.betsRepo = betsRepo;
        }

        public int GetTotalBetsCount()
        {
            return this.betsRepo.All().Count();
        }
    }
}
