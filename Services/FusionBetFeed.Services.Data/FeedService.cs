﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using FusionBetFeed.Common;
using FusionBetFeed.Common.Mapping;
using FusionBetFeed.Common.XMLMappings;
using FusionBetFeed.Data.Common.Repositories;
using FusionBetFeed.Data.Models;
using FusionBetFeed.Web.Infrastructure.ViewModels.Feed;

namespace FusionBetFeed.Services.Data
{
    public class FeedService : IFeedService
    {
        private IDeletableEntityRepository<Sport> sportsRepo;
        private IDeletableEntityRepository<Match> matchesRepo;
        private IDeletableEntityRepository<Odd> oddsRepo;
        private IDeletableEntityRepository<Bet> betsRepo;
        private IDeletableEntityRepository<Event> eventsRepo;
        public FeedService(IDeletableEntityRepository<Sport> sportsRepo
            , IDeletableEntityRepository<Match> matchesRepo
            , IDeletableEntityRepository<Odd> oddsRepo
            , IDeletableEntityRepository<Bet> betsRepo
            , IDeletableEntityRepository<Event> eventsRepo)
        {
            this.sportsRepo = sportsRepo;
            this.matchesRepo = matchesRepo;
            this.oddsRepo = oddsRepo;
            this.betsRepo = betsRepo;
            this.eventsRepo = eventsRepo;
        }

        public int AddBet(BetModel betModel)
        {
            throw new NotImplementedException();
        }

        public int AddEvent(EventModel eventModel)
        {
            throw new NotImplementedException();
        }

        public int AddMatch(MatchModel matchModel)
        {
            throw new NotImplementedException();
        }

        public Task AddNewEvents(List<EventModel> newEvents)
        {
            throw new NotImplementedException();
        }

        public int AddOdd(OddModel oddModel)
        {
            throw new NotImplementedException();
        }



        public List<OddViewModel> GetBetOdds(int id)
        {
            var odds = this.oddsRepo.All()
                .Where(s => s.BetId == id)
                .To<OddViewModel>()
                .ToList();

            return odds;
        }

        public List<MatchDefaultBetOddsViewModel> GetNextDayMatches(int skip, int take)
        {



            var nextDayMatches = GetQuerableNextDayMatches()
                                 .Skip(skip)
                                 .Take(take)
                                 .ToList()
                                 .AsQueryable()
                                 .To<MatchViewModel>()
                                 .GroupBy(s => s.Name);

            List<MatchDefaultBetOddsViewModel> result = new List<MatchDefaultBetOddsViewModel>();
            foreach (var item in nextDayMatches)
            {
                MatchDefaultBetOddsViewModel groupedMatch = new MatchDefaultBetOddsViewModel();
                //match data
                groupedMatch.MatchName = item.FirstOrDefault().Name; // get the first name of any match from the group
                groupedMatch.MatchType = item.FirstOrDefault().MatchType;
                groupedMatch.MatchId = item.FirstOrDefault().Id;
                groupedMatch.StartDate = item.FirstOrDefault().StartDate;
                //bets data
                groupedMatch.BetName = item.FirstOrDefault().MainBet.Name;
                groupedMatch.IsLive = item.FirstOrDefault().MainBet.IsLive;
                //odds data
                groupedMatch.HomeWinValue = item.FirstOrDefault().MainBet.Odds.FirstOrDefault(s => s.Name == GlobalConstants.HOME_WIN_ODD_NAME).Value;
                groupedMatch.HomeWinSpecialBetValue = item.FirstOrDefault().MainBet.Odds.FirstOrDefault(s => s.Name == GlobalConstants.HOME_WIN_ODD_NAME).SpecialBetValue;


                groupedMatch.AwayWinValue = item.FirstOrDefault().MainBet.Odds.FirstOrDefault(s => s.Name == GlobalConstants.AWAY_WIN_ODD_NAME).Value;
                groupedMatch.AwayWinSpecialBetValue = item.FirstOrDefault().MainBet.Odds.FirstOrDefault(s => s.Name == GlobalConstants.AWAY_WIN_ODD_NAME).SpecialBetValue;


                groupedMatch.DrawWinValue = item.FirstOrDefault().MainBet.Odds.FirstOrDefault(s => s.Name == GlobalConstants.DRAW_WIN_ODD_NAME).Value;
                groupedMatch.DrawWinSpecialBetValue = item.FirstOrDefault().MainBet.Odds.FirstOrDefault(s => s.Name == GlobalConstants.DRAW_WIN_ODD_NAME).SpecialBetValue;


                result.Add(groupedMatch);

            }



            return result;
        }


        public IQueryable<Match> GetQuerableNextDayMatches()
        {
            //get datetime to be 3 hours in the past to include the live matches
            var now = DateTime.UtcNow;
            var next24Hours = DateTime.UtcNow.AddHours(24);

            //getting all the matches with bets (in this case the original bet 1,2,x bet) and group them
            var nextDayMatches = (from m in this.matchesRepo.All(new string[] { "Bets", "Bets.Odds" })
                                  join b in this.betsRepo.All() on m.Id equals b.MatchId
                                  join o in this.oddsRepo.All() on b.Id equals o.BetId
                                  where b.Name == GlobalConstants.MatchWinnerBetName
                                  && now < m.StartDate && m.StartDate < next24Hours
                                  orderby m.StartDate
                                  select m);

            return nextDayMatches;
        }

        public async Task InitialFeed(XmlSportsModel xmlSportsModel)
        {
            var sportDb = new Sport();
            sportDb.FeedId = int.Parse(xmlSportsModel.Sport.ID);
            sportDb.Name = xmlSportsModel.Sport.Name;

            //saving each event separatatly
            sportsRepo.Add(sportDb);
            await sportsRepo.SaveChangesAsync();

            foreach (var eventItem in xmlSportsModel.Sport.Event)
            {
                Event eventDb = CreateEvent(eventItem);

                foreach (var matchItem in eventItem.Match)
                {
                    Match matchtDb = CreateMatchWithBetsAndOdds(matchItem);

                    eventDb.Matches.Add(matchtDb);
                }


                sportDb.Events.Add(eventDb);

                //saving each event separatatly
                await sportsRepo.SaveChangesAsync();
            }

        }

        private static Event CreateEvent(EventModel eventItem)
        {
            var eventDb = new Event();
            eventDb.IsLive = eventItem.IsLive == "true";
            eventDb.Name = eventItem.Name;
            eventDb.FeedId = int.Parse(eventItem.ID);
            eventDb.FeedCategoryId = eventItem.CategoryID;
            return eventDb;
        }

        private static Match CreateMatchWithBetsAndOdds(MatchModel matchItem)
        {
            var matchtDb = new Match();
            matchtDb.MatchType = (MatchType)Enum.Parse(typeof(MatchType), matchItem.MatchType);
            matchtDb.Name = matchItem.Name;
            matchtDb.FeedId = int.Parse(matchItem.ID);
            matchtDb.StartDate = DateTime.Parse(matchItem.StartDate);

            foreach (var betItem in matchItem.Bet)
            {
                Bet betDb = CreateBetWithOdds(betItem);

                matchtDb.Bets.Add(betDb);

            }

            return matchtDb;
        }

        private static Bet CreateBetWithOdds(BetModel betItem)
        {
            var betDb = new Bet();
            betDb.Name = betItem.Name;
            betDb.FeedId = int.Parse(betItem.ID);
            betDb.IsLive = betItem.IsLive == "true";


            foreach (var oddItem in betItem.Odd)
            {
                Odd oodDb = CreateOdd(oddItem);

                betDb.Odds.Add(oodDb);
            }

            return betDb;
        }

        private static Odd CreateOdd(OddModel oddItem)
        {
            var oodDb = new Odd();
            oodDb.Name = oddItem.Name;
            oodDb.FeedId = int.Parse(oddItem.ID);
            if (!string.IsNullOrEmpty(oddItem.SpecialBetValue))
            {
                oodDb.SpecialBetValue = decimal.Parse(oddItem.SpecialBetValue);
            }

            oodDb.Value = decimal.Parse(oddItem.Value);
            return oodDb;
        }

        public async Task AddUpdateEventsAndMatches(XmlSportsModel xmlSportsModel)
        {

            var now = DateTime.UtcNow;
            var next24Hours = DateTime.UtcNow.AddHours(24);
            //select the needed data outside of the loops
            var neededMatches = this.matchesRepo.All(new string[] { "Bets", "Bets.Odds" }).Where(m =>
                                (now < m.StartDate && m.StartDate < next24Hours)
                                //this is because of Match [ Name=UEFA Champions League 2018/19 - Winner ID=1217216 StartDate=2019-03-06T20:00:01 MatchType=OutRight ],which changes from day to day
                                || m.MatchType == MatchType.OutRight && m.Bets.Count > 0)
                                 .ToList();
            var allEvents = this.eventsRepo.All().ToList();
            int sportId = int.Parse(xmlSportsModel.Sport.ID);
            var sport = this.sportsRepo.All().FirstOrDefault(s => s.FeedId == sportId);

            foreach (var eventItem in xmlSportsModel.Sport.Event)
            {
                var eventFeedId = int.Parse(eventItem.ID);
                var oldEvent = allEvents.FirstOrDefault(s => s.FeedId == eventFeedId);
                Event eventDb = oldEvent != null ? oldEvent : CreateEvent(eventItem);

                if (eventDb.Id == 0)
                {

                    eventsRepo.Add(eventDb);
                    eventDb.SportId = sport.Id;
                    await this.eventsRepo.SaveChangesAsync();
                }


                foreach (var matchItem in eventItem.Match)
                {

                    var matchDate = DateTime.Parse(matchItem.StartDate);


                    //updating/adding only matches in the next 24 hours
                    if (now < matchDate && matchDate < next24Hours)
                    {
                        var oldMatchFeedId = int.Parse(matchItem.ID);
                        var oldMatch = neededMatches.FirstOrDefault(s => s.FeedId == oldMatchFeedId);
                        Match matchtDb = oldMatch != null ? oldMatch : CreateMatchWithBetsAndOdds(matchItem);

                        if (matchtDb.Id == 0)
                        {
                            //adding new match
                            matchtDb.EventId = eventDb.Id;
                            eventDb.Matches.Add(matchtDb);

                        }
                        else
                        {
                            //update the old match

                            matchtDb.MatchType = (MatchType)Enum.Parse(typeof(MatchType), matchItem.MatchType);
                            matchtDb.Name = matchItem.Name;
                            matchtDb.StartDate = DateTime.Parse(matchItem.StartDate);

                            foreach (var betItem in matchItem.Bet)
                            {
                                int betFeedId = int.Parse(betItem.ID);
                                bool betExists = matchtDb.Bets.Any(s => s.FeedId == betFeedId);
                                if (betExists)
                                {
                                    //update the bet and odds
                                    var bet = matchtDb.Bets.FirstOrDefault(s => s.FeedId == betFeedId);
                                    bet.Name = betItem.Name;
                                    bet.IsLive = betItem.IsLive == "true";

                                    foreach (var oddItem in betItem.Odd)
                                    {
                                        var oddFeedId = int.Parse(oddItem.ID);
                                        var oldOdd = bet.Odds.FirstOrDefault(s => s.FeedId == oddFeedId);
                                        var oddDb = oldOdd != null ? oldOdd : CreateOdd(oddItem);
                                        if (oddDb.Id == 0)
                                        {
                                            //add the odd
                                            bet.Odds.Add(oddDb);
                                        }
                                        else
                                        {
                                            //update the special value and value of the odd
                                            if (!string.IsNullOrEmpty(oddItem.SpecialBetValue))
                                            {
                                                oldOdd.SpecialBetValue = decimal.Parse(oddItem.SpecialBetValue);
                                            }

                                            oldOdd.Value = decimal.Parse(oddItem.Value);
                                        }


                                    }

                                }
                                else
                                {
                                    //adding the new bet with it's odds
                                    matchtDb.Bets.Add(CreateBetWithOdds(betItem));
                                }

                            }

                            matchesRepo.Update(matchtDb);
                        }



                    }


                }

                await matchesRepo.SaveChangesAsync();



            }
        }

        public List<MatchModel> GetMatchesForUpdate(XmlSportsModel newFeed, bool fullUpdate)
        {
            var matches = new List<MatchModel>();

            foreach (var ev in newFeed.Sport.Event)
            {
                foreach (var m in ev.Match)
                {
                    //to be fully corrent, this should include and some time in the past
                    var now = DateTime.UtcNow;
                    var next24Hours = DateTime.UtcNow.AddHours(24);
                    var matchDate = DateTime.Parse(m.StartDate);

                    if (!fullUpdate)
                    {
                        //if not full update update only next day matches with there main bet
                        if (now < matchDate && matchDate < next24Hours)
                        {
                            matches.Add(m);
                            //m.Bet.RemoveAll(s => s.Name != GlobalConstants.MatchWinnerBetName);
                        }
                    }
                    else
                    {
                        if (m.Bet.Count > 0)
                        {
                            matches.Add(m);
                            // m.Bet.RemoveAll(s => s.Name == GlobalConstants.MatchWinnerBetName);
                        }
                    }

                }
            }

            return matches;
        }

        public FullMatchViewModel GetMatchById(int id)
        {
            var match = this.matchesRepo.All(new string[] { "Bets", "Bets.Odds" }).FirstOrDefault(s => s.Id == id);
            var result = Mapper.Map<FullMatchViewModel>(match);
            return result;

        }
    }
}
