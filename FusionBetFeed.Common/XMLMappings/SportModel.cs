﻿using System;
using System.Xml.Serialization;
using System.Collections.Generic;


namespace FusionBetFeed.Common.XMLMappings
{
    [XmlRoot(ElementName = "Sport")]
    public class SportModel : FeedEntity
    {
        public SportModel()
        {
            this.Event = new List<EventModel>();
        }

        [XmlElement(ElementName = "Event")]
        public List<EventModel> Event { get; set; }
      
    }
}
