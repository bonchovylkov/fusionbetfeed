﻿using System;
using System.Xml.Serialization;
using System.Collections.Generic;


namespace FusionBetFeed.Common.XMLMappings
{
    [XmlRoot(ElementName = "Event")]
    public class EventModel : FeedEntity
    {
        public EventModel()
        {
            this.Match = new List<MatchModel>();
        }

        [XmlElement(ElementName = "Match")]
        public List<MatchModel> Match { get; set; }
        [XmlAttribute(AttributeName = "IsLive")]
        public string IsLive { get; set; }
        [XmlAttribute(AttributeName = "CategoryID")]
        public string CategoryID { get; set; }
    }
}
