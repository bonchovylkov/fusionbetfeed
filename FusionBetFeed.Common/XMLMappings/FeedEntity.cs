﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace FusionBetFeed.Common.XMLMappings
{
    public abstract class FeedEntity
    {
        [XmlAttribute(AttributeName = "Name")]
        public string Name { get; set; }
        [XmlAttribute(AttributeName = "ID")]
        public string ID { get; set; }
    }
}
