﻿
using System.Xml.Serialization;


namespace FusionBetFeed.Common.XMLMappings
{
    [XmlRoot(ElementName = "Odd")]
    public class OddModel : FeedEntity
    {

        [XmlAttribute(AttributeName = "Value")]
        public string Value { get; set; }
        [XmlAttribute(AttributeName = "SpecialBetValue")]
        public string SpecialBetValue { get; set; }
    }
}
