﻿using System;
using System.Xml.Serialization;
using System.Collections.Generic;


namespace FusionBetFeed.Common.XMLMappings
{
    [XmlRoot(ElementName = "Match")]
    public class MatchModel : FeedEntity
    {
        public MatchModel()
        {
            this.Bet = new List<BetModel>();
        }

        [XmlElement(ElementName = "Bet")]
        public List<BetModel> Bet { get; set; }

        [XmlAttribute(AttributeName = "StartDate")]
        public string StartDate { get; set; }
        [XmlAttribute(AttributeName = "MatchType")]
        public string MatchType { get; set; }
    }
}
