﻿
using System.Xml.Serialization;
using System.Collections.Generic;


namespace FusionBetFeed.Common.XMLMappings
{
    [XmlRoot(ElementName = "Bet")]
    public class BetModel : FeedEntity
    {
        public BetModel()
        {
            this.Odd = new List<OddModel>();
        }

        [XmlElement(ElementName = "Odd")]
        public List<OddModel> Odd { get; set; }

        [XmlAttribute(AttributeName = "IsLive")]
        public string IsLive { get; set; }
    }
}
