﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace FusionBetFeed.Common.Extentions
{
    public static class StringExtentions
    {
        public static string Sha256Hash(this string text)
        {

            StringBuilder Sb = new StringBuilder();

            using (var hash = SHA256.Create())
            {
                Encoding enc = Encoding.UTF8;
                Byte[] result = hash.ComputeHash(enc.GetBytes(text));

                foreach (Byte b in result)
                    Sb.Append(b.ToString("x2"));
            }

            return Sb.ToString();

        }
    }
}
