﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace FusionBetFeed.Common.Serialization
{
   public class XmlSerializerWrapper
    {
        public static T DeserializeXmlByTypeAndPath<T>(string path = null)
        {
            XmlSerializer des = new XmlSerializer(typeof(T));
            TextReader textReader = new StreamReader(path);
            T data;

            data = (T)des.Deserialize(textReader);

            textReader.Close();

            return data;
        }

        public static T DeserializeXmlByType<T>(string xml)
        {
            XmlSerializer des = new XmlSerializer(typeof(T));

            T data;

            using (TextReader reader = new StringReader(xml.Trim()))
            {
                data = (T)des.Deserialize(reader);
            }

            return data;
        }

      
    }
}
