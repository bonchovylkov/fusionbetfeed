﻿namespace FusionBetFeed.Common
{
    public static class GlobalConstants
    {

        public const string HOME_WIN_ODD_NAME = "1";
        public const string AWAY_WIN_ODD_NAME = "2";
        public const string DRAW_WIN_ODD_NAME = "x";

        public const string MatchWinnerBetName = "Match Winner ";

        public const string AdministratorRoleName = "Administrator";

        public const string JsonContentType = "application/json";

        public const string FUSION_BET_URL = "https://sports.ultraplay.net/sportsxml?clientKey={0}&sportId={1}";

        public const string FUSION_BET_FULL_FEED = "FUSION_BET_FULL_FEED";
        public const string FUSION_BET_FULL_FEED_SHA256 = "FUSION_BET_FULL_FEED_SHA256";

        public const string CACHE_KEYS_FUSION_MATCH_BY_ID_FOR_REMOVE = "CACHE_KEYS_FUSION_MATCH_BY_ID_FOR_REMOVE";

        public const string FUSION_MATCH_BY_ID = "FUSION_MATCH_BY_ID_{0}";

        public const string FUSION_NEXT_DAY_MATCHES = "FUSION_NEXT_DAY_MATCHES";

        public const string FUSION_BET_ODDS_BY_ID = "FUSION_BET_ODDS_BY_ID_{0}";

        public const string FUSION_BET_INITIAL_FEED_BETS_COUNT = "FUSION_BET_INITIAL_FEED_BETS_COUNT";



        public const int FEED_SKIP = 0;
        public const int FEED_TAKE = 10000;

        public const string SIGNAL_R_MATCH_GROUP = "SIGNAL_R_MATCH_GROUP";
    }
}
