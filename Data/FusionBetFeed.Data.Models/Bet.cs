﻿using FusionBetFeed.Data.Common.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FusionBetFeed.Data.Models
{
    public class Bet : BaseDeletableModel<int>, IFeedEntity
    {
        public Bet()
        {
            this.Odds = new HashSet<Odd>();
        }

        public ICollection<Odd> Odds { get; set; }
        public string Name { get; set; }
        public int FeedId { get; set; }
        public bool IsLive { get; set; }

        [Required]
        public int MatchId { get; set; }
        public Match Match { get; set; }
    }
}