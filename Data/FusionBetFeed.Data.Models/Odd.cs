﻿using FusionBetFeed.Data.Common.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FusionBetFeed.Data.Models
{
    public class Odd : BaseDeletableModel<int>,IFeedEntity
    {
        public string Name { get; set; }
        public int FeedId { get; set; }
        public decimal Value { get; set; }
        public decimal? SpecialBetValue { get; set; }

        [Required]
        public int BetId { get; set; }
        public Bet Bet { get; set; }
    }
}