﻿using FusionBetFeed.Data.Common.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace FusionBetFeed.Data.Models
{
    public class Sport : BaseDeletableModel<int>, IFeedEntity
    {
        public Sport()
        {
            this.Events = new HashSet<Event>();
        }

        public ICollection<Event> Events { get; set; }
        public string Name { get; set; }
        public int FeedId { get; set; }
    }
}
