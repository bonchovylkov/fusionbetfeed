﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FusionBetFeed.Data.Models
{
   public interface IFeedEntity
    {
        int FeedId { get; set; }
        string Name { get; set; }
    }
}
