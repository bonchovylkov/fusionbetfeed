﻿using FusionBetFeed.Data.Common.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FusionBetFeed.Data.Models
{
    public class Event : BaseDeletableModel<int>, IFeedEntity
    {
        public Event()
        {
            this.Matches = new HashSet<Match>();
        }

        public ICollection<Match> Matches { get; set; }
        public string Name { get; set; }
        public int FeedId { get; set; }
        public bool IsLive { get; set; }
        public string FeedCategoryId { get; set; }

        [Required]
        public int SportId { get; set; }
        public Sport Sport { get; set; }
    }
}