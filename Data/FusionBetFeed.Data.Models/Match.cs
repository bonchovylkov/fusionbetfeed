﻿using FusionBetFeed.Data.Common.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FusionBetFeed.Data.Models
{
    public class Match : BaseDeletableModel<int>, IFeedEntity
    {
        public Match()
        {
            this.Bets = new HashSet<Bet>();
        }

        public ICollection<Bet> Bets { get; set; }
        public string Name { get; set; }
        public int FeedId { get; set; }
        [Required]
        public DateTime StartDate { get; set; }
        public MatchType MatchType { get; set; }

        public int EventId { get; set; }
        public Event Event { get; set; }
    }
}

