﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace FusionBetFeed.Data.Migrations
{
    public partial class MakeSpecialValueDeciaml : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "SpecialBetValue",
                table: "Odds",
                nullable: true,
                oldClrType: typeof(decimal));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "SpecialBetValue",
                table: "Odds",
                nullable: false,
                oldClrType: typeof(decimal),
                oldNullable: true);
        }
    }
}
