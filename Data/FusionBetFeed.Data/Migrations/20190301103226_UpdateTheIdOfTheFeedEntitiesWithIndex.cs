﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace FusionBetFeed.Data.Migrations
{
    public partial class UpdateTheIdOfTheFeedEntitiesWithIndex : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "FeedID",
                table: "Odds",
                newName: "FeedId");

            migrationBuilder.AlterColumn<int>(
                name: "FeedId",
                table: "Sports",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "FeedId",
                table: "Odds",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "FeedId",
                table: "Matches",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "FeedId",
                table: "Events",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "FeedId",
                table: "Bets",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Sports_FeedId",
                table: "Sports",
                column: "FeedId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Odds_FeedId",
                table: "Odds",
                column: "FeedId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Matches_FeedId",
                table: "Matches",
                column: "FeedId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Events_FeedId",
                table: "Events",
                column: "FeedId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Bets_FeedId",
                table: "Bets",
                column: "FeedId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Sports_FeedId",
                table: "Sports");

            migrationBuilder.DropIndex(
                name: "IX_Odds_FeedId",
                table: "Odds");

            migrationBuilder.DropIndex(
                name: "IX_Matches_FeedId",
                table: "Matches");

            migrationBuilder.DropIndex(
                name: "IX_Events_FeedId",
                table: "Events");

            migrationBuilder.DropIndex(
                name: "IX_Bets_FeedId",
                table: "Bets");

            migrationBuilder.RenameColumn(
                name: "FeedId",
                table: "Odds",
                newName: "FeedID");

            migrationBuilder.AlterColumn<string>(
                name: "FeedId",
                table: "Sports",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<string>(
                name: "FeedID",
                table: "Odds",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<string>(
                name: "FeedId",
                table: "Matches",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<string>(
                name: "FeedId",
                table: "Events",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<string>(
                name: "FeedId",
                table: "Bets",
                nullable: true,
                oldClrType: typeof(int));
        }
    }
}
