﻿namespace FusionBetFeed.Data.Seeding
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using FusionBetFeed.Common;
    using FusionBetFeed.Common.Serialization;
    using FusionBetFeed.Common.XMLMappings;
    using FusionBetFeed.Data.Models;
    using FusionBetFeed.Services.Data;
    using FusionBetFeed.Web.Infrastructure.Http;
    using FusionBetFeed.Web.Infrastructure.ViewModels.Settings;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Options;

    public static class ApplicationDbContextSeeder
    {
        public static async Task Seed(ApplicationDbContext dbContext, IServiceProvider serviceProvider)
        {
            if (dbContext == null)
            {
                throw new ArgumentNullException(nameof(dbContext));
            }

            if (serviceProvider == null)
            {
                throw new ArgumentNullException(nameof(serviceProvider));
            }

            var roleManager = serviceProvider.GetRequiredService<RoleManager<ApplicationRole>>();
            SeedIdentity(dbContext, roleManager);

            //var feedService = serviceProvider.GetRequiredService<IFeedService>();
            //var httpService = serviceProvider.GetRequiredService<IHttpRequester>();
            //var fusionOptions = serviceProvider.GetRequiredService<IOptions<FusionBetSettings>>();
            //SeedFeed(dbContext, feedService, httpService, fusionOptions).Wait();
        }

        private static async Task SeedFeed(ApplicationDbContext dbContext, IFeedService feedService, IHttpRequester httpRequester, IOptions<FusionBetSettings> options)
        {
            //seed only if the database is empty
            if (dbContext.Sports.Count() == 0)
            {
                var url = string.Format(GlobalConstants.FUSION_BET_URL, options.Value.ApiKey, options.Value.SportId);
                var xml = await httpRequester.GetContent(url);
                var data = XmlSerializerWrapper.DeserializeXmlByType<XmlSportsModel>(xml);
                feedService.InitialFeed(data).Wait();
            }
        }

        public static void SeedIdentity(ApplicationDbContext dbContext, RoleManager<ApplicationRole> roleManager)
        {
            if (dbContext == null)
            {
                throw new ArgumentNullException(nameof(dbContext));
            }

            if (roleManager == null)
            {
                throw new ArgumentNullException(nameof(roleManager));
            }

            SeedRoles(roleManager);
        }

        private static void SeedRoles(RoleManager<ApplicationRole> roleManager)
        {
            SeedRole(GlobalConstants.AdministratorRoleName, roleManager);
        }

        private static void SeedRole(string roleName, RoleManager<ApplicationRole> roleManager)
        {
            var role = roleManager.FindByNameAsync(roleName).GetAwaiter().GetResult();
            if (role == null)
            {
                var result = roleManager.CreateAsync(new ApplicationRole(roleName)).GetAwaiter().GetResult();

                if (!result.Succeeded)
                {
                    throw new Exception(string.Join(Environment.NewLine, result.Errors.Select(e => e.Description)));
                }
            }
        }
    }
}
