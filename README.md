﻿## How to run the project

0.Should have VS 2017 with .NET CORE 2.1 Installed (min),Installed Node,Installed Gulp with (npm install gulp or npm install gulp -g), should have SQL server express (if not express change connection sting from ./SQLEXPRESS to ./)

1. Clone the project..
2. Open the folder containing the package.json file located in *fusionbetfeed\Web\FusionBetFeed.Web
3. Open CMD and run "npm install" - wait for a few minutes 
4. Open the project with Visual Studio 2017
5. Run gulp dev - can be run from the console of preferably from the Task Runner Explorer of Visual Studio  (can be found under View->Other windows)
 5.1 To be faster run gulp release (which contatenates and minifies all js and html files)
6. Start the application (this should create the database) and seed the initial data on the first run