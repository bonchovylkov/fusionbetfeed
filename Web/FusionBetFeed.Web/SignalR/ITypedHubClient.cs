﻿using FusionBetFeed.Web.Infrastructure.ViewModels.Feed;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FusionBetFeed.Web.SignalR
{
    public interface ITypedHubClient
    {
        Task BroadcastMatches(IEnumerable<MatchDefaultBetOddsViewModel> matches);
        Task BroadcastMatch(FullMatchViewModel match);
    }
}
