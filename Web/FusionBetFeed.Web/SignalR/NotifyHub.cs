﻿using FusionBetFeed.Common;
using Microsoft.AspNetCore.SignalR;
using System.Security.Claims;
using System.Threading.Tasks;

namespace FusionBetFeed.Web.SignalR
{

   
    public class NotifyHub : Hub<ITypedHubClient>
    {

        
        public async Task SubscribeForMatchUpdate(int matchId)
        {

            await Groups.AddToGroupAsync(Context.ConnectionId, GlobalConstants.SIGNAL_R_MATCH_GROUP + matchId);
        }

    }
}
