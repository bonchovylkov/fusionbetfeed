﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FusionBetFeed.Web.Infrastructure.ViewModels.Settings;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;

using System.Net.Http;
using FusionBetFeed.Common;
using FusionBetFeed.Web.Infrastructure.Http;
using FusionBetFeed.Services.Data;
using FusionBetFeed.Services;
using FusionBetFeed.Web.Infrastructure.ViewModels.Feed;
using FusionBetFeed.Common.XMLMappings;
using FusionBetFeed.Common.Serialization;
using Newtonsoft.Json;
using FusionBetFeed.Common.Extentions;
using FusionBetFeed.Web.SignalR;
using Microsoft.AspNetCore.SignalR;

namespace FusionBetFeed.Web.Controllers
{
    [Route("api/ProxyFeed")]
    public class ProxyFeedController : BaseController
    {
        private ICacheService cache;
        private FusionBetSettings fusionBetSettings;
        private IHttpRequester requester;
        private IFeedService feedService;
        private ISportService sportService;
        private IBetsService betsService;


        private IHubContext<NotifyHub, ITypedHubClient> _hubContext;


        public ProxyFeedController(ICacheService cache,
            IHubContext<NotifyHub, ITypedHubClient> hubContext,
            IOptions<FusionBetSettings> settings,
            IFeedService feedService,
            IHttpRequester requester,
            ISportService sportService,
            IBetsService betsService)
        {
            this.cache = cache;
            this.fusionBetSettings = settings.Value;
            this.requester = requester;
            this.feedService = feedService;
            this.sportService = sportService;
            this.betsService = betsService;
            this._hubContext = hubContext;
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("CheckInitialFeed")]
        public async Task<IActionResult> CheckInitialFeed()
        {

            bool needSeed = sportService.GetSportByFeedId(fusionBetSettings.SportId) == null;

            return Ok(new { needSeed = needSeed });
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("CheckInitialFeedProgress")]
        public async Task<IActionResult> CheckInitialFeedProgress()
        {
            decimal progress = 0;

            int? totalBets = this.cache.Get<int>(GlobalConstants.FUSION_BET_INITIAL_FEED_BETS_COUNT);
            if (totalBets != null && totalBets.HasValue && totalBets.Value > 0)
            {
                decimal savedRecords = this.betsService.GetTotalBetsCount();
                progress = (savedRecords / totalBets.Value) * 100;
            }

            return Ok(new { progress = progress });
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("InitialFeed")]
        public async Task<IActionResult> InitialFeed()
        {
            //seed only if the database is empty, for double check
            if (sportService.GetSportByFeedId(fusionBetSettings.SportId) == null)
            {
                var url = string.Format(GlobalConstants.FUSION_BET_URL, fusionBetSettings.ApiKey, fusionBetSettings.SportId);
                var xml = await requester.GetContent(url);
                var data = XmlSerializerWrapper.DeserializeXmlByType<XmlSportsModel>(xml);
                SetTotalRecordsCountInCache(data);
                await feedService.InitialFeed(data);
            }
            return Ok();
        }

        /// <summary>
        /// Set the total number of the initial feed bets, to be able to get the % of it
        /// </summary>
        /// <param name="data">Initial feed value</param>
        private void SetTotalRecordsCountInCache(XmlSportsModel data)
        {
            int betsCount = 0;
            foreach (var ev in data.Sport.Event)
            {
                foreach (var m in ev.Match)
                {
                    betsCount += m.Bet.Count;
                }
            }

            cache.Add<int>(GlobalConstants.FUSION_BET_INITIAL_FEED_BETS_COUNT, betsCount);
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("CheckFeedForUpdate")]
        public async Task<IActionResult> CheckFeedForUpdate(int? matchId)
        //public IActionResult CheckFeedForUpdate()
        {
            var url = string.Format(GlobalConstants.FUSION_BET_URL, this.fusionBetSettings.ApiKey, this.fusionBetSettings.SportId);
            var xml = await this.requester.GetContent(url);
            var newFeed = XmlSerializerWrapper.DeserializeXmlByType<XmlSportsModel>(xml);


            //changes detection
            var cacheAlldataHash = this.cache.Get<string>(GlobalConstants.FUSION_BET_FULL_FEED_SHA256);

            //serialize to json to reduce the size of the string, but only the sport, because the wrapper have some timestamp, which is updated
            var newAllDataAsString = JsonConvert.SerializeObject(newFeed.Sport);
            var newAllDataAsStringSha256 = newAllDataAsString.Sha256Hash();

            //update only if there is difference in the feed
            if (cacheAlldataHash != newAllDataAsStringSha256)
            {

                await feedService.AddUpdateEventsAndMatches(newFeed);
                //feedService.AddUpdateEventsAndMatches(newFeed).Wait();


                //the case where we update all matches by signal R
                if (matchId == null)
                {
                    var nextDayMatches = feedService.GetNextDayMatches(GlobalConstants.FEED_SKIP, GlobalConstants.FEED_TAKE);
                    //pushing the data to all connected clients
                    _hubContext.Clients.All.BroadcastMatches(nextDayMatches);
                    //then set the items in the cache for 60 minutes
                    cache.Add<List<MatchDefaultBetOddsViewModel>>(GlobalConstants.FUSION_NEXT_DAY_MATCHES, nextDayMatches, 60);
                }
                else // case where we want to update certain match
                {
                    var match = this.feedService.GetMatchById(matchId.Value);
                    //update the cache
                    var cacheKey = string.Format(GlobalConstants.FUSION_MATCH_BY_ID, matchId);
                    cache.Add<FullMatchViewModel>(cacheKey, match, 10);

                    this._hubContext.Clients.Groups(GlobalConstants.SIGNAL_R_MATCH_GROUP + matchId).BroadcastMatch(match);
                 
                }

                //adding in the cache the new sha256 hash of the data, to be able to check it next time for changes
                this.cache.Add<string>(GlobalConstants.FUSION_BET_FULL_FEED_SHA256, newAllDataAsStringSha256);



            }

            return Ok();
        }




        [AllowAnonymous]
        [HttpGet]
        [Route("GetNextDayMatches")]
        public async Task<IActionResult> GetNextDayMatches(int skip = GlobalConstants.FEED_SKIP, int take = GlobalConstants.FEED_TAKE)
        {
            //try to get them from cache
            List<MatchDefaultBetOddsViewModel> nextDayMatches = null;
            nextDayMatches = this.cache.Get<List<MatchDefaultBetOddsViewModel>>(GlobalConstants.FUSION_NEXT_DAY_MATCHES);
            if (nextDayMatches == null)
            {
                //if they are not set in the cache we get them from the DB
                nextDayMatches = feedService.GetNextDayMatches(skip, take);

                //then set the items in the cache for 60 minutes
                cache.Add<List<MatchDefaultBetOddsViewModel>>(GlobalConstants.FUSION_NEXT_DAY_MATCHES, nextDayMatches, 60);

            }


            return Ok(nextDayMatches);
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("GetBetOdds/{id}")]
        public async Task<IActionResult> GetBetOdds(int id)
        {
            //TODO: think about if need to cache the odds, because they change to frequently

            //try to get them from cache
            List<OddViewModel> betOdds = null;
            betOdds = this.cache.Get<List<OddViewModel>>(string.Format(GlobalConstants.FUSION_BET_ODDS_BY_ID, id));
            if (betOdds == null)
            {
                //if they are not set in the cache we get them from the DB
                betOdds = feedService.GetBetOdds(id);

                //then set the items in the cache for 10 minutes
                cache.Add<List<OddViewModel>>(string.Format(GlobalConstants.FUSION_BET_ODDS_BY_ID, id), betOdds, 10);

            }


            return Ok(betOdds);
        }


        [AllowAnonymous]
        [HttpGet]
        [Route("GetMatchById/{id}")]
        public async Task<IActionResult> GetMatchById(int id)
        {


            //try to get them from cache
            FullMatchViewModel match = null;
            var cacheKey = string.Format(GlobalConstants.FUSION_MATCH_BY_ID, id);

            match = this.cache.Get<FullMatchViewModel>(cacheKey);
            if (match == null)
            {
                //if they are not set in the cache we get them from the DB
                match = feedService.GetMatchById(id);

                //then set the items in the cache for 10 minutes
                cache.Add<FullMatchViewModel>(cacheKey, match, 10);




            }


            return Ok(match);
        }
    }
}