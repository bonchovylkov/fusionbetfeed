﻿'use strict';

(function (global) {
    // where to look for things
    var map = {
        '@angular': 'lib/@angular',
        '@angular/common/http': 'lib/@angular/common/bundles/common-http.umd.min.js',
        '@angular/animations': 'lib/@angular/animations/bundles/animations.umd.min.js',
        '@angular/animations/browser': 'lib/@angular/animations/bundles/animations-browser.umd.min.js',
        '@angular/platform-browser/animations': 'lib/@angular/platform-browser/bundles/platform-browser-animations.umd.min.js',
        '@aspnet/signalr': 'lib/@aspnet',
        'app': 'app',
        'rxjs': 'lib/rxjs',
        'tslib': 'lib/tslib/tslib.js',
        'zone.js': 'lib/zone.js/dist',
        'primeng': 'lib/primeng/js/primeng.js', // add the main wrapper
        'primeng/button': 'lib/primeng',
        'primeng/panel': 'lib/primeng/js/components/panel',
        'primeng/dataview': 'lib/primeng/js/components/dataview',
        'primeng/datalist': 'lib/primeng/js/components/datalist',
        'primeng/progressbar': 'lib/primeng/js/components/progressbar'

      
    };

    // how to load when no filename and/or no extension
    var packages = {
        'app': { main: 'main', defaultExtension: 'js' },
        'rxjs': { defaultExtension: 'js' },
        'zone.js': { main: 'zone', defaultExtension: 'js' },
        '@aspnet/signalr': { main: 'js/signalr.min.js', defaultExtension: 'js' },   
        'primeng/dataview': { main: 'dataview.js', defaultExtension: 'js' },
        'primeng/datalist': { main: 'datalist.js', defaultExtension: 'js' },
        'primeng/progressbar': { main: 'progressbar.js', defaultExtension: 'js' },
        'primeng/button': { main: 'js/components/button/button.js', defaultExtension: 'js' },
        'primeng/panel': { main: 'panel.js', defaultExtension: 'js' }


    };

    [
        //'animations',
        'core',
        'common',
        'compiler',
        'forms',
        'platform-browser',
        'platform-browser-dynamic',
        'router'
    ].forEach(function (packageName) {
        packages['@angular/' + packageName] = {
            main: 'bundles/' + packageName + '.umd.min.js',
            defaultExtension: 'js'
        };
    });

    System.config({
        map: map,
        packages: packages
    });
})(this);
