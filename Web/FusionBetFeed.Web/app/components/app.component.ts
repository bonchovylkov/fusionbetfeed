﻿/// <reference path="../../node_modules/@types/core-js/index.d.ts" />

import { Component } from '@angular/core';
import { SignalrService } from '../services/index';

@Component({
    moduleId: module.id,
    selector: 'app-root',
    templateUrl: 'app.component.html'
})

export class AppComponent {
    constructor(private signalRService: SignalrService) {

    }

    ngOnInit() {
        if (!this.signalRService.checkIfConnectionStatusIsOpen()) {
            //if the connection is not openned we open it
            this.signalRService.startConnection();
        }
    }
}
