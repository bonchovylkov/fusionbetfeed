﻿import { Component } from '@angular/core';
import { MenuItem } from 'primeng/components/common/menuitem';
import { ProxyFeedDataService } from '../services/data/proxy-feed-data.service';
import { SignalrService } from '../services/index';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    moduleId: module.id,
    selector: 'bets',
    templateUrl: 'bets.component.html'
})

export class BetsComponent {

    private activeRoute: any;
    id: number;
    match = null;
    minute = 60000;

    constructor(private proxyService: ProxyFeedDataService,
                private currentRoute: Router,
        private route: ActivatedRoute
        , private signalRService: SignalrService) {

    }



    ngOnInit() {

        this.activeRoute = this.route.params.subscribe(params => {

            this.id = +params['id']; // (+) converts string 'id' to a number

            this.proxyService.getMatchById(this.id)
                .subscribe(result => {
                    this.match = result;

                    this.checkFeedForUpdate(this.id);

                    //creating group for this match
                    this.signalRService.signalRConn.send("SubscribeForMatchUpdate", this.id)
                        .then(() => {
                            
                            console.log("Successfully adding " + this.id);

                        });

                    this.setDataHandler();

                });

        });
    }

    setDataHandler() {
        console.log("Subscribe for match update");

        this.signalRService.signalRConn.on('BroadcastMatch', (data) => {
            this.match = data;
        });
    }


    checkFeedForUpdate(matchId) {
        var self = this;



        //check with long pooing for updates in the feed
        var updateInterval = setInterval(function () {


            self.proxyService.checkFeedForUpdate(matchId)
                .subscribe((result) => {

                    //updating the data here would be the same as the  with the SignalR
                    //the other option without long pooling for check for update was to create Timer in the backend

                });

        }, self.minute);

    }
    
}
