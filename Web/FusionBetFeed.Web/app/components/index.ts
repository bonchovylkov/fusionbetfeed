﻿import { AppComponent } from './app.component';
import { HomeComponent } from './home.component';
import { BetsComponent } from './bets.component';

export * from './app.component';
export * from './home.component';
export * from './bets.component'

export const APP_COMPONENTS = [
    AppComponent,
    HomeComponent,
    BetsComponent
];
