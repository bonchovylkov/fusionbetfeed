﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { AppFooterComponent } from './theme/app-footer.component';
import { AppHeaderComponent } from './theme/app-header.component';

import { APP_DIRECTIVES } from '../../directives/index';
import { APP_PIPES } from '../../pipes/index';

import { ButtonModule } from 'primeng/button';
import { PanelModule } from 'primeng/panel';
import { DataViewModule } from 'primeng/dataview';
import { DataListModule } from 'primeng/datalist';
import { ProgressBarModule } from 'primeng/progressbar';


//import { SplitButtonModule } from 'primeng/splitbutton';
//import { CalendarModule } from 'primeng/calendar';
//import { InplaceModule } from 'primeng/inplace';
//import { TableModule } from 'primeng/table';
//import { TooltipModule } from 'primeng/tooltip';
//import { GrowlModule } from 'primeng/growl';
//import { FieldsetModule } from 'primeng/fieldset';
//import { AccordionModule } from 'primeng/accordion';
//import { DataGridModule } from 'primeng/datagrid';
//import { CardModule } from 'primeng/card';
//import { ToolbarModule } from 'primeng/toolbar';
//import { DropdownModule } from 'primeng/dropdown';
//import { OverlayPanelModule } from 'primeng/overlaypanel';
//import { AutoCompleteModule } from 'primeng/autocomplete';
//import { DragDropModule } from 'primeng/dragdrop';
//import { DialogModule } from 'primeng/dialog';
//import { DataTableModule } from 'primeng/datatable';
//import { TabViewModule } from 'primeng/tabview';
//import { MegaMenuModule } from 'primeng/megamenu';
//import { SliderModule } from 'primeng/slider';
//import { StepsModule } from 'primeng/steps';
//import { ToggleButtonModule } from 'primeng/togglebutton';
//import { ScrollPanelModule } from 'primeng/scrollpanel';
//import { ChartModule } from 'primeng/chart';
//import { TriStateCheckboxModule } from 'primeng/tristatecheckbox';
//import { EditorModule } from 'primeng/editor';
//import { ChipsModule } from 'primeng/chips';



@NgModule({
    imports: [CommonModule, FormsModule, RouterModule,
        ButtonModule, PanelModule, DataViewModule, DataListModule, ProgressBarModule,

    ],
    declarations: [
        AppFooterComponent,
        AppHeaderComponent,

        APP_DIRECTIVES,
        APP_PIPES
    ],
    exports: [
        CommonModule,
        FormsModule,
        RouterModule,


        ButtonModule, PanelModule, ProgressBarModule, DataListModule, DataViewModule,

        AppFooterComponent,
        AppHeaderComponent,

        APP_DIRECTIVES,
        APP_PIPES
    ]
})

export class AppSharedModule { }
