﻿import { Component } from '@angular/core';
import { MenuItem } from 'primeng/components/common/menuitem';
import { ProxyFeedDataService } from '../services/data/proxy-feed-data.service';
import { SignalrService } from '../services/index';

@Component({
    moduleId: module.id,
    selector: 'home',
    templateUrl: 'home.component.html'
})

export class HomeComponent {

    constructor(private proxyService: ProxyFeedDataService
        , private signalRService: SignalrService) {

    }
    initialFeedProgress: number = 0;
    seedInitiated: boolean = false;
    nextDayMatches: any = null;
    minute = 60000;

    ngOnInit() {

        var self = this;
        //check for initial seed logic or to display the data
        this.proxyService.checkInitialFeed()
            .subscribe((data) => {
                //start seeding
                if (data.needSeed) {
                    this.seedInitiated = true;

                    this.proxyService.seedInitiated()
                        .subscribe(() => {
                            console.log("seed initiated");

                        });


                    //do that with long pooling
                    var progressInterval = setInterval(function () {


                        self.proxyService.checkInitialFeedProgress()
                            .subscribe((result) => {

                                var progressUpdate = Math.floor(result.progress);
                                self.initialFeedProgress = progressUpdate;
                                if (self.initialFeedProgress >= 100) {
                                    self.seedInitiated = false;
                                    clearInterval(progressInterval);
                                    self.getNextDayMatches();

                                    //wait for a minute and check for update

                                    self.checkFeedForUpdate();

                                }

                            });

                    }, 3000);
                }
                else {
                    this.getNextDayMatches();



                    self.checkFeedForUpdate();

                }
            });


    }

    setDataHandler() {
        console.log("Subscribe for matches updates");
        this.signalRService.signalRConn.on('BroadcastMatches', (data) => {
            this.nextDayMatches = data;
        });
    }

    getNextDayMatches(): any {
        this.proxyService.getNextDayMatches()
            .subscribe((result) => {
                this.nextDayMatches = result;


                //subscribing for data changes
                this.setDataHandler();
            });
    }




    checkFeedForUpdate() {
        var self = this;



        //check with long pooing for updates in the feed
        var updateInterval = setInterval(function () {


            self.proxyService.checkFeedForUpdate(null)
                .subscribe((result) => {

                    //updating the data here would be the same as the  with the SignalR
                    //the other option without long pooling for check for update was to create Timer in the backend

                });

        }, self.minute);

    }
}
