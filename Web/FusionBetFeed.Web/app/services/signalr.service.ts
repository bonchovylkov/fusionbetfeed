﻿import { Injectable } from '@angular/core';
import * as signalR from "@aspnet/signalr";



//declare var self: any;

@Injectable()
export class SignalrService {

    public signalRConn: any = new signalR.HubConnectionBuilder().withUrl("/notify").build();
    public userAddedToGroup = false;

    constructor() {
        
    }

    public startConnection(succsessCallback = null) {


        this.signalRConn
            .start()
            .then(() => {
                console.log('Connection started!');
                if (succsessCallback) {
                    succsessCallback();
                }

            })
            .catch(err => {
                console.log('Error while establishing connection :(')
            });


        //if the connection is closed by some reason different than manual close of the connection
        //we try to reconnect, on each 5 seconds
        this.signalRConn.onclose((data) => {
            if (data) {
                var self = this;
                var attempts = 0;
                var intervarId = setInterval(() => {

                    attempts++;
                    //if the connection is opened  and the user is authorized  yet  I try to do it
                    self.signalRConn
                        .start()
                        .then(() => {
                            console.log('Connection restarted after unexpected stop!');

                        })
                        .catch(err => {
                            console.log('Error while establishin reconnection :(');

                        });

                    if (attempts >= 10) {
                        clearInterval(intervarId);
                    }

                }, 5000);

            }
        })



    }


    public closeConnection() {
        this.signalRConn.stop();
      
    }

    public checkIfConnectionStatusIsOpen() {
        var isOpen = false;
        if (this.signalRConn) {
            isOpen = this.signalRConn.state == signalR.HubConnectionState.Connected;
        }

        return isOpen;
    }

}
