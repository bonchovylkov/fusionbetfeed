﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';


@Injectable()
export class ProxyFeedDataService {
    private static readonly URLS = {
        CHECK_INITIAL_FEED: 'api/ProxyFeed/CheckInitialFeed',
        CHECK_INITIAL_FEED_PROGRESS: 'api/ProxyFeed/CheckInitialFeedProgress',
        SET_INITIAL_FEED: 'api/ProxyFeed/InitialFeed',
        GET_NEXT_DAY_MATCHES: 'api/proxyfeed/GetNextDayMatches',
        GET_CHECK_FOR_UPDATES: 'api/proxyfeed/CheckFeedForUpdate?matchId=',
        GET_MATCH_BY_ID:'api/proxyfeed/GetMatchById/'
    };

    constructor(private httpClient: HttpClient) { }

    public checkInitialFeed(): Observable<any> {
        return this.httpClient.get(ProxyFeedDataService.URLS.CHECK_INITIAL_FEED);
    }

    public checkInitialFeedProgress(): Observable<any> {
        return this.httpClient.get(ProxyFeedDataService.URLS.CHECK_INITIAL_FEED_PROGRESS);
    }

    public seedInitiated(): Observable<any> {
        return this.httpClient.get(ProxyFeedDataService.URLS.SET_INITIAL_FEED);
    }


    public getNextDayMatches(): Observable<any> {
        return this.httpClient.get(ProxyFeedDataService.URLS.GET_NEXT_DAY_MATCHES);
    }


    public checkFeedForUpdate(matchId): Observable<any> {
        return this.httpClient.get(`${ProxyFeedDataService.URLS.GET_CHECK_FOR_UPDATES}${matchId}`);
    }


    public getMatchById(id:number): Observable<any> {
        return this.httpClient.get(`${ProxyFeedDataService.URLS.GET_MATCH_BY_ID}${id}`);
    }
    
    

}
