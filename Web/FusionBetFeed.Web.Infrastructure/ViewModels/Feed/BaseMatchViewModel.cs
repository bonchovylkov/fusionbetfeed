﻿using FusionBetFeed.Common.Mapping;
using FusionBetFeed.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace FusionBetFeed.Web.Infrastructure.ViewModels.Feed
{
    public class BaseMatchViewModel : BaseViewModel, IMapFrom<Match>
    {
        public string Name { get; set; }
        public int FeedId { get; set; }

        public DateTime StartDate { get; set; }
        public MatchType MatchType { get; set; }
    }
}
