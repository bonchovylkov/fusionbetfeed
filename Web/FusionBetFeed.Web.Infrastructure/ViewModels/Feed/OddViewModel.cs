﻿using FusionBetFeed.Common.Mapping;
using FusionBetFeed.Data.Models;

namespace FusionBetFeed.Web.Infrastructure.ViewModels.Feed
{
    public class OddViewModel : BaseViewModel, IMapFrom<Odd>
    {
        public string Name { get; set; }
        public int FeedId { get; set; }
        public decimal Value { get; set; }
        public decimal? SpecialBetValue { get; set; }

        
        public int BetId { get; set; }
        
    }
}