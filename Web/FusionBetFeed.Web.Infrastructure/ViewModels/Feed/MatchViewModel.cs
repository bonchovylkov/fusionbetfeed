﻿using AutoMapper;
using FusionBetFeed.Common;
using FusionBetFeed.Common.Mapping;
using FusionBetFeed.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FusionBetFeed.Web.Infrastructure.ViewModels.Feed
{
    public class MatchViewModel : BaseMatchViewModel, IMapFrom<Match>, IHaveCustomMappings
    {
        //public MatchViewModel()
        //{
        //    this.Bets = new HashSet<BetViewModel>();
        //}

        // public IEnumerable<BetViewModel> Bets { get; set; }

        public BetViewModel MainBet { get; set; }


        //public decimal HomeWinOdd { get; set; }
        //public decimal DrawOdd { get; set; }
        //public decimal AwayWinOdd { get; set; }

        //public void CreateMappings(IMapperConfigurationExpression configuration)
        //{
        //    configuration.CreateMap<Match, MatchViewModel>()
        //   .ForMember(m => m.HomeWinOdd, opt => opt.MapFrom(t => t.Bets.ElementAt(0).Odds.FirstOrDefault(p=>p.Name =="1").Value))
        //   .ForMember(m => m.DrawOdd, opt => opt.MapFrom(t => t.Bets.ElementAt(0).Odds.FirstOrDefault(p => p.Name == "x").Value))
        //   .ForMember(m => m.AwayWinOdd, opt => opt.MapFrom(t => t.Bets.ElementAt(0).Odds.FirstOrDefault(p => p.Name == "2").Value));
        //}

        public void CreateMappings(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<Match, MatchViewModel>()
           .ForMember(m => m.MainBet, opt => opt.MapFrom(t => t.Bets.FirstOrDefault(s=>s.Name == GlobalConstants.MatchWinnerBetName)));
        }
    }
}
