﻿using FusionBetFeed.Common.Mapping;
using FusionBetFeed.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace FusionBetFeed.Web.Infrastructure.ViewModels.Feed
{
    public class FullMatchViewModel : BaseMatchViewModel, IMapFrom<Match>
    {
        public FullMatchViewModel()
        {
            this.Bets = new HashSet<BetViewModel>();
        }

        public IEnumerable<BetViewModel> Bets { get; set; }
    }
}
