﻿using FusionBetFeed.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace FusionBetFeed.Web.Infrastructure.ViewModels.Feed
{
    public class MatchDefaultBetOddsViewModel
    {
        public int MatchId { get; set; }
        public string MatchName { get; set; }
        

        public DateTime StartDate { get; set; }
        public MatchType MatchType { get; set; }

        public string BetName { get; set; }
        public bool IsLive { get; set; }

        
        public decimal HomeWinValue { get; set; }
        public decimal? HomeWinSpecialBetValue { get; set; }


        public decimal AwayWinValue { get; set; }
        public decimal? AwayWinSpecialBetValue { get; set; }


        public decimal DrawWinValue { get; set; }
        public decimal? DrawWinSpecialBetValue { get; set; }


    }
}
