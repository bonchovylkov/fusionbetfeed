﻿using FusionBetFeed.Common.Mapping;
using FusionBetFeed.Data.Models;
using System.Collections.Generic;

namespace FusionBetFeed.Web.Infrastructure.ViewModels.Feed
{
    public class BetViewModel : BaseViewModel, IMapFrom<Bet>
    {
        public BetViewModel()
        {
            this.Odds = new HashSet<OddViewModel>();
        }

        public IEnumerable<OddViewModel> Odds { get; set; }
        public string Name { get; set; }
        public int FeedId { get; set; }
        public bool IsLive { get; set; }

        
        public int MatchId { get; set; }
        
    }
}