﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FusionBetFeed.Web.Infrastructure.ViewModels.Settings
{
    public class FusionBetSettings
    {
        public int SportId { get; set; }
        public string ApiKey { get; set; }
    }
}
