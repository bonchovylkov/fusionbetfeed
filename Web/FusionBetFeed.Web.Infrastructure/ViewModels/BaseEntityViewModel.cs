﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FusionBetFeed.Web.Infrastructure.ViewModels
{
    public class DetailedBaseEntityViewModel : BaseViewModel
    {
        public DateTime CreatedOn { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public string CreatedOnFormatted
        {
            get
            {
                return CreatedOn.ToString("yyyy-MM-ddTHH:mm:ss.fffzzz");
            }
        }
    }
}
