﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace FusionBetFeed.Web.Infrastructure.Http
{
    public class HttpRequester : IHttpRequester
    {
        private readonly IHttpClientFactory _clientFactory;
        public HttpRequester(IHttpClientFactory clientFactory)
        {
            this._clientFactory = clientFactory;
        }
        public async Task<string> GetContent(string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, url);

            var client = _clientFactory.CreateClient();

            var response = await client.SendAsync(request);

            if (response.IsSuccessStatusCode)
            {
                var result = await response.Content
                    .ReadAsStringAsync();
                return result;
            }
            else
            {
                throw new ArgumentException($"Url -{url} returns response - {response.StatusCode}, additional info - {response.Content}");
            }
        }

        //public async Task<T> GetContent<T>(string url)
        //{
        //    var request = new HttpRequestMessage(HttpMethod.Get, url);

        //    var client = _clientFactory.CreateClient();

        //    var response = await client.SendAsync(request);

        //    if (response.IsSuccessStatusCode)
        //    {
        //        var result = await response.Content
        //            .ReadAsAsync<T>();
        //        return result;
        //    }
        //    else
        //    {
        //        throw new ArgumentException($"Url -{url} returns response - {response.StatusCode}, additional info - {response.Content}");
        //    }
        //}

        public Task<string> Post(string url, object data)
        {
            throw new NotImplementedException();
        }
    }
}
