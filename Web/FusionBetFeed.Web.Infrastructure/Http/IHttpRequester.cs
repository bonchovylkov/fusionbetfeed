﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FusionBetFeed.Web.Infrastructure.Http
{
    public interface IHttpRequester
    {
        Task<string> GetContent(string url);
        //Task<T> GetContent<T>(string url);
        Task<string> Post(string url, object data);
    }
}
